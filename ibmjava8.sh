#!/bin/bash

CURRDIR=$(pwd)
JAVA_DL=$(echo "http://public.dhe.ibm.com/ibmdl/export/pub/systems/cloud/runtimes/java/8.0.6.36/linux/x86_64/ibm-java-x86_64-sdk-8.0-6.36.bin")
JAVA_TMP=(ibm-java-x86_64-sdk-8.0-6.36.bin)
HOME_DIR=($HOME)

trap 'rm -f $CURRDIR/$JAVA_TMP' EXIT HUP INT TERM

pre_req() {
#Check existence of Java on the node
if [ -d /opt/ibm/java-x86_64-80/bin ]; then
echo "IBM Java 8 is already installed on the host >&2" && exit 1
else
echo "IBM JAva 8 is not present on the node, continuing..."
fi
sudo yum update -y &>/dev/null
if [ $? -eq 0 ]; then
echo "dnf update success"
else
echo "dnf update failed >&2" && exit 1
fi
RPM_CHECK=$(sudo rpm -qa wget | wc -l)
if [ "$RPM_CHECK" -eq 1 ]; then
echo "wget already installed"
else
sudo yum install wget -y &>/dev/null
if [ $? -ne 0 ]; then
echo "installation of wget failed" >&2; exit 1
else
echo "installation of wget completed"
fi
fi
}

install() {
# Set vars,download & install the package
echo "Downloading the package, please wait..."
sudo wget $JAVA_DL &>/dev/null
if [ $? -eq 0 ]; then
echo "java downloaded successfully"
else
echo "java download failed >&2" && exit 1
fi
export _JAVA_OPTIONS="-Dlax.debug.level=3 -Dlax.debug.all=true"
export LAX_DEBUG=1
cat <<'EOF' >> installer.properties
INSTALLER_UI=silent
USER_INSTALL_DIR=/opt/ibm/java-x86_64-80
LICENSE_ACCEPTED=TRUE
EOF
chmod +x $CURRDIR/ibm-java-x86_64-sdk-8.0-6.36.bin && $CURRDIR/ibm-java-x86_64-sdk-8.0-6.36.bin -i silent -f installer.properties
if [ $? -ne 0 ]; then
echo "installation of IBM Java failed >&2" && exit 1
else
echo "installation of IBM Java completed"
fi
}

post_install(){
grep -i java $HOME_DIR/.bash_profile &>/dev/null
if [ $? -eq 0 ]; then
echo "PATH already configured"
else
echo "export PATH=/opt/ibm/java-x86_64-80/bin:$PATH" >> $HOME_DIR/.bash_profile && source $HOME_DIR/.bash_profile
fi
java -version &>/dev/null
if [ $? -eq 0 ]; then
echo "Java post-check and config completed"
else
echo "Java post-check and config failed >&2" && exit 1
fi
}
